﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

namespace GromDatabase
{
	public class GromSettingsWindow : EditorWindow
	{

		private const int WINDOW_WIDTH = 400;
		private const int WINDOW_HEIGHT = 300;
		private const int SMALL_BTN_WIDTH = 25;

		private const string SUB_PATHS_HINT = 
			"These file system partial paths (separated by \"/\") " +
			"are used when searching in the Assets folder for CS-files containing Enums. " +
			"File paths which are not containing sub-paths from the list are ignored.";
		private const string NAMESPACES_HINT = 
			"Specify namespaces (format: NamespaceA.NamespaceB.NamespaceC) for the Enums searching procedure.";

		// scaled window width
		private static int Ws(float factor = 0.95f)
		{
			return Convert.ToInt32(Mathf.Floor(factor * WINDOW_WIDTH));
		}

		private GromSettings settingsAsset;

		[MenuItem("Window/GromDatabase/SettingsWindow")]
		public static void CreateSettingsWindow()
		{
			GromSettingsWindow window = EditorWindow.GetWindow<GromSettingsWindow>("Grom Settings");
			window.minSize = new Vector2(WINDOW_WIDTH, WINDOW_HEIGHT);
		}

		void OnEnable()
		{
			EnumSearcher.ClearCache();

			FilesystemHelper.CreateSettingsAsset();
			settingsAsset = (GromSettings)AssetDatabase.LoadAssetAtPath(DataPathHelper.RelativeAssetsSettingsPath, typeof(GromSettings));
		}



		void OnGUI()
		{
			if(settingsAsset == null)
				return;
			
			if(EditorApplication.isCompiling == false)
			{
				EditorGUILayout.BeginVertical();
				EditorGUILayout.Space();
				DisplayEnumSubPaths();
				EditorGUILayout.Space();
				DisplayNamespaces();
				EditorGUILayout.EndVertical();
				Validate();
			}
			else
			{
				EditorGUILayout.HelpBox("Files are compiling please wait few seconds.", MessageType.Info);
			}
		}

		void Validate()
		{

			for(int i = 0; i < settingsAsset.namespaces.Count; ++i)
			{
				if(!NamespasesPathValidator.Validate(settingsAsset.namespaces[i]))
				{
					settingsAsset.namespaces[i] = "";
				}
			}
		}

		void BtnAdd(List<string> items)
		{
			if(GUILayout.Button("+", GUILayout.Width(SMALL_BTN_WIDTH), GUILayout.ExpandWidth(false)))
			{
				items.Add("");
			}
		}

		void DisplayEnumSubPaths()
		{
			DisplayList("Enum sub-paths", SUB_PATHS_HINT, settingsAsset.subpaths);
		}

		void DisplayNamespaces()
		{
			DisplayList("Namespaces", NAMESPACES_HINT, settingsAsset.namespaces);
		}

		void DisplayList(string title, string hint, List<string> items)
		{
			EditorGUILayout.BeginHorizontal();
			BtnAdd(items);
			EditorGUILayout.LabelField(title, EditorStyles.boldLabel, GUILayout.ExpandWidth(false));
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.LabelField(hint, 
				EditorStyles.wordWrappedMiniLabel,
				GUILayout.Width(Ws())
			);

			int removeMe = -1;
			int i = 0;
			foreach(string s in items)
			{
				EditorGUILayout.BeginHorizontal("Box");

				items[i] = EditorGUILayout.TextField(s, GUILayout.Width(Ws() - SMALL_BTN_WIDTH));

				if(GUILayout.Button("-", GUILayout.Width(SMALL_BTN_WIDTH), GUILayout.ExpandWidth(false)))
				{
					removeMe = i;
				}
				EditorGUILayout.EndHorizontal();
				++i;
			}

			if(removeMe >= 0)
			{
				items.RemoveAt(removeMe);
			}
		}
	}
}

