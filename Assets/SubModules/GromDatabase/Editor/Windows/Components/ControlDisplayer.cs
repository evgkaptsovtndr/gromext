﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;

namespace GromDatabase
{

	public class ControlDisplayer
	{

		private GromSettings settings;
		IDBRefAccessor refsAccessor;

		public ControlDisplayer(GromSettings settings, IDBRefAccessor refsAccessor = null)
		{
			this.settings = settings;
			this.refsAccessor = refsAccessor;
		}

		public void DisplayControl(
			IDBRefAccessor dbRef, 
			DatabaseContentManager.FieldInfoEx field, 
			ref object item, 
			int fieldSize, 
			bool isEditView = false
		)
		{
			if(field.field.FieldType.IsEnum)
			{
				item = EditorGUILayout.EnumPopup((Enum)item, GUILayout.Width(fieldSize));
				return;
			}
			else if(field.data.key == NodeType.listType)
			{
				CreateListField(field.field, ref item, fieldSize, isEditView);
				return;
			}
			else if(field.data.key == NodeType.recordReference)
			{
				string[] captions;
				int[] values;

				if(dbRef != null)
				{
					dbRef.GetRecordReferences(field.data.extType, out captions, out values);
				}
				else
				{
					captions = new string[]{ };
					values = new int[]{ };
				}

				RecordRefField(captions, values, ref item, fieldSize);
				    
				return;
			}
					
			EditFieldFromTypeString(field.field.FieldType.ToString(), ref item, fieldSize, isEditView);
		}



		private void CreateListField(FieldInfo field, ref object aList, int fieldSize, bool isEditView)
		{
			if(aList == null)
			{
				aList = Activator.CreateInstance(field.FieldType);
			}
			
			IList lst = (IList)aList;

			EditorGUILayout.BeginVertical();

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Size: ", GUILayout.Width(50), GUILayout.ExpandWidth(false));
			int newSize = EditorGUILayout.IntField(lst.Count, GUILayout.Width(50), GUILayout.ExpandWidth(false));
			EditorGUILayout.EndHorizontal();

			Type itemType = GenericUtils.GetTemplateArg(aList);

			GenericUtils.Resize(lst, newSize);

			for(int i = 0; i < lst.Count; ++i)
			{
				object val = lst[i];
				if(
					// try to display editor for a standard type
					EditFieldFromTypeString(itemType.FullName, ref val, fieldSize, isEditView) ||
					// ... for a enum
					EnumEditFieldFromTypeString(itemType.FullName, ref val, fieldSize) ||
					// .. for a table record reference
					TableEditFieldFromTypeString(itemType.FullName, ref val, fieldSize))
				{
					lst[i] = val;
				}
			}

			EditorGUILayout.EndVertical();
		}

		private bool TableEditFieldFromTypeString(string dbRecordTypeName, ref object item, int fieldSize)
		{
			if(refsAccessor == null)
			{
				Debug.LogWarning("Table references accessor in null at Schema editing.");
				return false;
			}

			if(!TableRecordRefsUtils.IsTableReferenceType(dbRecordTypeName))
				return false;

			string dbName = TableRecordRefsUtils.TypeFromTableReferenceType(dbRecordTypeName);
	
			int[] values;
			string[] captions;

			refsAccessor.GetRecordReferences(dbName, out captions, out values);

			if(captions == null)
				return false;

			object itemIdRef = TableRecordRefsUtils.GetRecordId(item);

			if(!RecordRefField(captions, values, ref itemIdRef, fieldSize))
				return true;

			int itemId = (int)itemIdRef;
			if(itemId < 0)
				return false;

			IDbItem dbItem = (IDbItem)refsAccessor.GetRecordById(dbName, itemId, typeof(IDbItem));
			if(dbItem == null)
				return false;

			TableRecordRefsUtils.SetRecordId(ref item, dbItem, dbRecordTypeName);

			return true;
		}


		private bool RecordRefField(string[] captions, int[] values, ref object item, int fieldSize)
		{
			if(captions != null)
			{
				item = EditorGUILayout.IntPopup((int)item, captions, values, GUILayout.Width(fieldSize));
				return true;
			}
			else
			{
				EditorGUILayout.LabelField("<no records>", GUILayout.Width(fieldSize));
				item = 0;
				return false;
			}
		}


		private bool EnumEditFieldFromTypeString(string type, ref object item, int fieldSize)
		{
			foreach(string enumTypeName in EnumSearcher.SearchForEnums(settings))
			{
				if(enumTypeName == type)
				{
					item = EditorGUILayout.EnumPopup((Enum)item);
					return true;
				}

			}
			
			return false;
		}

		private bool EditFieldFromTypeString(string type, ref object item, int fieldSize, bool isEditView)
		{
			switch(type)
			{
				case "System.Char":
					item = CharField("", (char)item, fieldSize);
					break;
				case "System.Byte":
					item = ByteField("", (byte)item, fieldSize);
					break;
				case "System.Int32":
					item = EditorGUILayout.IntField((int)item, GUILayout.Width(fieldSize));
					break;
					#if UNITY_5_0 || UNITY_5_1
					case "System.Int64":
					item = EditorGUILayout.LongField((long)item, GUILayout.Width(fieldSize));
					break;
					#endif
				case "System.Single":
					item = EditorGUILayout.FloatField((float)item, GUILayout.Width(fieldSize));
					break;
				case "System.Double":
					#if UNITY_5_0 || UNITY_5_1
					item = EditorGUILayout.DoubleField((double)item, GUILayout.Width(fieldSize));
					#endif
					#if UNITY_4_6
					item = DoubleField("", (double)item, fieldSize);
					#endif
					break;
				case "System.String":
					if(isEditView)
					{
						item = EditorGUILayout.TextArea((string)item, GUILayout.Height(100), GUILayout.Width(fieldSize));
					}
					else
					{
						item = EditorGUILayout.TextField((string)item, GUILayout.Width(fieldSize));
					}
					break;
				case "System.Boolean":
					item = EditorGUILayout.Toggle((bool)item, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.Vector2":
					item = EditorGUILayout.Vector2Field("", (Vector2)item, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.Vector3":
					item = EditorGUILayout.Vector3Field("", (Vector3)item, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.Vector4":
					item = EditorGUILayout.Vector4Field("Values:", (Vector4)item, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.Color":
					//item = EditorGUI.ColorField(new Rect(0, 0, 0, 0), (Color)item);
					item = EditorGUILayout.ColorField("", (Color)item, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.GameObject":
					item = EditorGUILayout.ObjectField((GameObject)item, typeof(GameObject), true, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.AnimationCurve":
					if(item == null)
					{
						item = new AnimationCurve();
					}
					item = EditorGUILayout.CurveField((AnimationCurve)item, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.AnimationClip":
					item = EditorGUILayout.ObjectField((AnimationClip)item, typeof(AnimationClip), true, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.AudioClip":
					item = EditorGUILayout.ObjectField((AudioClip)item, typeof(AudioClip), true, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.Bounds":
					item = EditorGUILayout.BoundsField((Bounds)item, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.LayerMask":
					item = LayerMaskField("", (LayerMask)item, fieldSize);
					break;
				case "UnityEngine.Material":
					item = EditorGUILayout.ObjectField((Material)item, typeof(Material), true, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.Rect":
					item = EditorGUILayout.RectField((Rect)item, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.Sprite":
					item = EditorGUILayout.ObjectField((Sprite)item, typeof(Sprite), true, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.Texture":
					item = EditorGUILayout.ObjectField((Texture)item, typeof(Texture), true, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.Texture2D":
					item = EditorGUILayout.ObjectField((Texture2D)item, typeof(Texture2D), true, GUILayout.Width(fieldSize));
					break;
				case "UnityEngine.Transform":
					item = EditorGUILayout.ObjectField((Transform)item, typeof(Transform), true, GUILayout.Width(fieldSize));
					break;
				default:
					return false;
			}

			return true;
		}




		private T CreateDefOnDemand<T>(SchemaNodeData node)
		{
			if(node.defValue == null || !node.defValue.GetType().Equals(typeof(T)))
			{
				node.defValue = default(T);
			}
			return (T)node.defValue;
		}

		private object CreateDefOnDemand(SchemaNodeData node, Type t)
		{
			if(node.defValue == null || !node.defValue.GetType().Equals(t))
			{
				node.defValue = t.IsValueType ? Activator.CreateInstance(t) : null;
			}
			return node.defValue;
		}

		public void DisplayControl(
			SchemaNodeData node, 
			int fieldSize
		)
		{
			if(node.key == NodeType.enumType)
			{
				EditorGUILayout.LabelField("-", GUILayout.Width(fieldSize));
				return;
			}

			if(node.key == NodeType.recordReference)
			{
				EditorGUILayout.LabelField("NULL", GUILayout.Width(fieldSize));
				return;
			}

			if(!node.IsSerializable)
			{
				EditorGUILayout.LabelField("-", GUILayout.Width(fieldSize));
				return;
			}
				
			switch(node.key)
			{
				case NodeType.charType:
					
					node.defValue = CharField("", CreateDefOnDemand<char>(node), fieldSize);
					break;
				case NodeType.byteType:
					node.defValue = ByteField("", CreateDefOnDemand<byte>(node), fieldSize);
					break;
				case NodeType.intType:
					node.defValue = EditorGUILayout.IntField(CreateDefOnDemand<int>(node), GUILayout.Width(fieldSize));
					break;
					#if UNITY_5_0 || UNITY_5_1
					case "System.Int64":
					item = EditorGUILayout.LongField(CreateDefOnDemand<long>(node), GUILayout.Width(fieldSize));
					break;
					#endif
				case NodeType.floatType:
					node.defValue = EditorGUILayout.FloatField(CreateDefOnDemand<float>(node), GUILayout.Width(fieldSize));
					break;
				case NodeType.doubleType:
					#if UNITY_5_0 || UNITY_5_1
					node.defValue = EditorGUILayout.DoubleField(CreateDefOnDemand<double>(node), GUILayout.Width(fieldSize));
					#elif UNITY_4_6
					node.defValue = DoubleField("",CreateDefOnDemand<double>(node), fieldSize);
					#else
					node.defValue = DoubleField("", CreateDefOnDemand<double>(node), fieldSize);
					#endif
					break;
				case NodeType.stringType:
					node.defValue = EditorGUILayout.TextField(CreateDefOnDemand<string>(node), GUILayout.Width(fieldSize));
					break;
				case NodeType.boolType:
					node.defValue = EditorGUILayout.Toggle(CreateDefOnDemand<bool>(node), GUILayout.Width(fieldSize));
					break;
				case NodeType.vector2Type:
					node.defValue = EditorGUILayout.Vector2Field("", CreateDefOnDemand<Vector2>(node), GUILayout.Width(fieldSize));
					break;
				case NodeType.vector3Type:
					node.defValue = EditorGUILayout.Vector3Field("", CreateDefOnDemand<Vector3>(node), GUILayout.Width(fieldSize));
					break;
				case NodeType.vector4Type:
					node.defValue = EditorGUILayout.Vector4Field("Values:", CreateDefOnDemand<Vector4>(node), GUILayout.Width(fieldSize));
					break;
				case NodeType.colorType:
					//item = EditorGUI.ColorField(new Rect(0, 0, 0, 0), (Color)item);
					node.defValue = EditorGUILayout.ColorField("", CreateDefOnDemand<Color>(node), GUILayout.Width(fieldSize));
					break;
				case NodeType.gameObjectType:
					node.defValue = EditorGUILayout.ObjectField(CreateDefOnDemand<GameObject>(node), typeof(GameObject), true, GUILayout.Width(fieldSize));
					break;
				case NodeType.animationCurveType:
					node.defValue = EditorGUILayout.CurveField(CreateDefOnDemand<AnimationCurve>(node), GUILayout.Width(fieldSize));
					break;
				case NodeType.animationClipType:
					node.defValue = EditorGUILayout.ObjectField(CreateDefOnDemand<AnimationClip>(node), typeof(AnimationClip), true, GUILayout.Width(fieldSize));
					break;
				case NodeType.audioClipType:
					node.defValue = EditorGUILayout.ObjectField(CreateDefOnDemand<AudioClip>(node), typeof(AudioClip), true, GUILayout.Width(fieldSize));
					break;
				case NodeType.boundsType:
					node.defValue = EditorGUILayout.BoundsField(CreateDefOnDemand<Bounds>(node), GUILayout.Width(fieldSize));
					break;
				case NodeType.layerMaskType:
					node.defValue = LayerMaskField("", CreateDefOnDemand<LayerMask>(node), fieldSize);
					break;
				case NodeType.materialType:
					node.defValue = EditorGUILayout.ObjectField(CreateDefOnDemand<Material>(node), typeof(Material), true, GUILayout.Width(fieldSize));
					break;
				case NodeType.rectType:
					node.defValue = EditorGUILayout.RectField(CreateDefOnDemand<Rect>(node), GUILayout.Width(fieldSize));
					break;
				case NodeType.spriteType:
					node.defValue = EditorGUILayout.ObjectField(CreateDefOnDemand<Sprite>(node), typeof(Sprite), true, GUILayout.Width(fieldSize));
					break;
				case NodeType.textureType:
					node.defValue = EditorGUILayout.ObjectField(CreateDefOnDemand<Texture>(node), typeof(Texture), true, GUILayout.Width(fieldSize));
					break;
				case NodeType.texture2DType:
					node.defValue = EditorGUILayout.ObjectField(CreateDefOnDemand<Texture2D>(node), typeof(Texture2D), true, GUILayout.Width(fieldSize));
					break;
			/*
				 	case "UnityEngine.Transform":
						item = EditorGUILayout.ObjectField((Transform)item, typeof(Transform), true, GUILayout.Width(fieldSize));
						break;
					*/
			}
		}


		private LayerMask LayerMaskField(string label, LayerMask layerMask, int fieldSize)
		{
			List<string> layers = new List<string>();
			List<int> layerNumbers = new List<int>();

			for(int i = 0; i < 32; i++)
			{
				string layerName = LayerMask.LayerToName(i);
				if(layerName != "")
				{
					layers.Add(layerName);
					layerNumbers.Add(i);
				}
			}
			int maskWithoutEmpty = 0;
			for(int i = 0; i < layerNumbers.Count; i++)
			{
				if(((1 << layerNumbers[i]) & layerMask.value) > 0)
					maskWithoutEmpty |= (1 << i);
			}
			maskWithoutEmpty = EditorGUILayout.MaskField(label, maskWithoutEmpty, layers.ToArray(), GUILayout.Width(fieldSize));
			int mask = 0;
			for(int i = 0; i < layerNumbers.Count; i++)
			{
				if((maskWithoutEmpty & (1 << i)) > 0)
					mask |= (1 << layerNumbers[i]);
			}
			layerMask.value = mask;
			return layerMask;
		}

		private byte ByteField(string label, byte byteValue, int fieldSize)
		{
			int intValue = (int)byteValue;
			intValue = EditorGUILayout.IntField((int)intValue, GUILayout.Width(fieldSize));

			if(intValue < 0)
			{
				intValue = 0;
			}
			else if(intValue > 255)
			{
				intValue = 255;
			}

			byteValue = (byte)intValue;

			return byteValue;
		}

		private char CharField(string label, char charValue, int fieldSize)
		{
			string stringValue = charValue.ToString();

			stringValue = EditorGUILayout.TextField(label, (string)stringValue, GUILayout.Width(fieldSize));

			if(stringValue.Length > 1)
			{
				stringValue = stringValue.Remove(1);
			}

			charValue = Convert.ToChar(stringValue);

			return charValue;
		}

		private double DoubleField(string label, double doubleValue, int fieldSize)
		{
			string stringValue = doubleValue.ToString();
			stringValue = EditorGUILayout.TextField(label, (string)stringValue, GUILayout.Width(fieldSize));

			double.TryParse(stringValue, out doubleValue);

			return doubleValue;
		}
	}

}