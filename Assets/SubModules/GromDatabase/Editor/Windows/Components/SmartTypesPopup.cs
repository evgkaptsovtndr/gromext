﻿using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace GromDatabase
{
	public class SmartTypesPopup
	{
		private SchemaList schemaListAsset;
		private GromSettings settingsAsset;

		public SmartTypesPopup(SchemaList schemaListAsset, GromSettings settingsAsset)
		{
			this.schemaListAsset = schemaListAsset;
			this.settingsAsset = settingsAsset;
		}

		public bool SmartChoose(SchemaNodeData node, int fieldSize)
		{
			if(node.key == NodeType.enumType)
			{
				EditorGUILayout.LabelField("Enum class: ", GUILayout.Width(fieldSize));
				node.extType = ChooseEnum(node.extType);
				return true;
			}
			else if(node.key == NodeType.recordReference)
			{
				EditorGUILayout.LabelField("Ref. table: ", GUILayout.Width(fieldSize));
				node.extType = ChooseDatabaseName(node.extType);
				return true;
			}
			else if(node.key == NodeType.listType)
			{
				EditorGUILayout.LabelField("Item. type: ", GUILayout.Width(fieldSize));
				node.extType = ChooseAnyType(node.extType);
				return true;
			}

			return false;
		}

		public string ChooseAnyType(string current)
		{
			List<string> types = NodeTypeInfo.GetTypeNameList().Select(name => "Standard/" + name).ToList();
			types.AddRange(EnumSearcher.SearchForEnums(settingsAsset).Select(name => "Enums/" + name));
			types.AddRange(GetDatabaseNameList().Select(name => "Table/" + name));

			string item = types[
				              EditorGUILayout.Popup(
					              Mathf.Max(0,
						              Mathf.Max(types.IndexOf("Enums/" + current),
							              Mathf.Max(types.IndexOf("Standard/" + current), 
								              types.IndexOf("Table/" + current)))),
					              types.ToArray())
			              ];

			return item.Substring(item.LastIndexOf("/") + 1);
		}

		public string ChooseEnum(string current)
		{
			List<string> enums = EnumSearcher.SearchForEnums(settingsAsset);
			return enums[
				EditorGUILayout.Popup(Mathf.Max(0, enums.IndexOf(current)), enums.ToArray())
			];
		}

		public string ChooseDatabaseName(string current)
		{

			List<string> names = GetDatabaseNameList();
			return names[
				EditorGUILayout.Popup(Mathf.Max(names.IndexOf(current), 0), names.ToArray())
			];

		}

		private List<string> GetDatabaseNameList()
		{
			List<string> names = new List<string>();
			for(int i = 0; i < schemaListAsset.Count; ++i)
			{
				SchemaItem s = schemaListAsset.GetElementByIndex<SchemaItem>(i);
				if(s != null)
				{
					names.Add(s.GetSchemaQualifiedName);
				}
			}
			return names;
		}


	}
}

