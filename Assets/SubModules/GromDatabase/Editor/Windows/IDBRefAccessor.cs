﻿using UnityEngine;
using System.Collections;

namespace GromDatabase
{

	public interface IDBRefAccessor
	{
		object GetRecordById(string dbName, int id, System.Type type);

		void GetRecordReferences(string dbName, out string[] captions, out int[] values);
	}
}

