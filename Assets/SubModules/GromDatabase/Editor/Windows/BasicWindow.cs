﻿using UnityEditor;
using UnityEngine;
using System.Collections;

namespace GromDatabase
{
	public class BasicWindow : EditorWindow
	{

		protected Vector2 scrollPosition = Vector2.zero;
		protected SchemaList schemaListAsset;
		protected GromSettings settingsAsset;

		protected virtual void OnEnable()
		{
			FilesystemHelper.CreateSettingsAsset();
			schemaListAsset = (SchemaList)AssetDatabase.LoadAssetAtPath(DataPathHelper.RelativeAssetsDatabaseSchemaPath, typeof(SchemaList));
			settingsAsset = (GromSettings)AssetDatabase.LoadAssetAtPath(DataPathHelper.RelativeAssetsSettingsPath, typeof(GromSettings));
		}
	}
}
