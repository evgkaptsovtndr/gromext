﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GromDatabase
{
	public class SchemaInfoView : BasicSchemaWindow
	{

		private static bool saveStatus = true;


		public static bool SaveStatus
		{
			get
			{
				return saveStatus;
			}
			set
			{
				saveStatus = value;
			}
		}

		public void DisplayInfo()
		{
			//NotSaveInfo();
			SchemaIsGeneratedInfo();
		}

		public void NotSaveInfo()
		{
			if(!saveStatus)
			{
				EditorGUILayout.HelpBox("Data is not save! \nAll changes will be lost after restart Unity.", MessageType.Warning);
			}
		}

		private void SchemaIsGeneratedInfo()
		{
			if(schemaListAsset.Count > 0)
			{
				if(!schemaListAsset.GetElementByIndex<SchemaItem>(selectedSchemaItem).isGenerated)
				{
					EditorGUILayout.HelpBox("Schema is not generated", MessageType.Warning);
				}
			}
		}

	}
}
