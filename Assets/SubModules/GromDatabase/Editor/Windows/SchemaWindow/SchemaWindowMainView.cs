using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


namespace GromDatabase
{
	public class SchemaWindowMainView : BasicSchemaWindow
	{

		private const int FIELD_WIDTH = 125;
		private const int FIELD_WIDTH_LARGE = 160;

		private SchemaItem schema = null;


		/// <summary>
		/// Displaies the main view with details.
		/// </summary>
		public void DisplayMain()
		{

			scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, "Box");

			if(schemaListAsset.Count == 0)
			{

				EditorGUILayout.HelpBox("Create new schema", MessageType.Info);
			}
			else
			{
				ViewSelectedSchema();
			}

			EditorGUILayout.EndScrollView();
		}

		/// <summary>
		/// Views the selected schema.
		/// </summary>
		private void ViewSelectedSchema()
		{
			schema = schemaListAsset.GetElementByIndex<SchemaItem>(selectedSchemaItem);

			Toolbar();
			EditorGUI.BeginChangeCheck();

			SchemaEditor();

			if(EditorGUI.EndChangeCheck())
			{
				SchemaInfoView.SaveStatus = false;
				DatabaseManagerHelper.SetIsGenerated(schemaListAsset, selectedSchemaItem, false);
			}
		}

		/// <summary>
		/// Display editor with schema data.
		/// </summary>
		private void SchemaEditor()
		{
            

			EditName();
			EditRecordsTableTitle();
            
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.BeginVertical();

			int i = 1;
			List<int> toAdd = new List<int>();
			List<int> toRemove = new List<int>();
			foreach(SchemaNodeData node in schema.Get)
			{
				if(i > 1)
				{
					EditNode(node, i, i > 2, toAdd, toRemove);
				}
				++i;
			}
			EditorGUILayout.EndVertical();
			EditorGUILayout.EndHorizontal();

			DoAdd(toAdd);
			DoRemove(toRemove);
		}



		/// <summary>
		/// Dispaly small toolbar on the top of editor schema.
		/// </summary>
		private void Toolbar()
		{
			EditorGUILayout.Space();
			EditorGUILayout.BeginHorizontal();
//			ButtonAdd(schema.Count, "Add new", FIELD_WIDTH);
			//SaveButton();
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Space();
		}

		private void HorizLine()
		{
			GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(1));
		}

		/// <summary>
		/// Edits the name of schema class.
		/// </summary>
		private void EditName()
		{
			EditorGUILayout.BeginHorizontal("Box");
			EditorGUILayout.LabelField("Schema name: ", GUILayout.Width(FIELD_WIDTH));

			string nameBefore = schema.GetSchemaName;
			schema.GetElement(0).value = EditorGUILayout.TextField(schema.GetSchemaName);
			if(!nameBefore.Equals(schema.GetElement(0).value))
			{
				ProcessTableNameChange(schema, nameBefore);
			}

			EditorGUILayout.EndHorizontal();

			HorizLine();
		}

		// Для сохранения целостности базы мы обновляем все ссылки на нее,
		// если ее имя меняется.
		private void ProcessTableNameChange(SchemaItem aSchema, string oldName)
		{
			for(var i = 0; i < schemaListAsset.Count; ++i)
			{
				SchemaItem table = schemaListAsset.GetElementByIndex<SchemaItem>(i);
				for(var k = 0; k < table.Count; ++k)
				{
					SchemaNodeData nodeData = table.GetElement(k);
					if(nodeData.key == NodeType.recordReference && nodeData.extType.Equals(oldName))
					{
						nodeData.extType = aSchema.GetSchemaName;
					}
				}
			}
		}

		private void EditRecordsTableTitle()
		{
			EditorGUILayout.BeginHorizontal("Box");
			EditorGUILayout.LabelField("Type", EditorStyles.boldLabel, GUILayout.Width(FIELD_WIDTH));
			EditorGUILayout.LabelField("Name", EditorStyles.boldLabel, GUILayout.Width(FIELD_WIDTH));
			EditorGUILayout.LabelField("Def.value", EditorStyles.boldLabel, GUILayout.Width(FIELD_WIDTH_LARGE));
			EditorGUILayout.LabelField("Ext.value", EditorStyles.boldLabel, GUILayout.ExpandWidth(true));
			EditorGUILayout.EndHorizontal();
		}

		private string StdTextField(string value)
		{
			return EditorGUILayout.TextField(value, GUILayout.ExpandWidth(false), GUILayout.Width(FIELD_WIDTH));
		}

		/// <summary>
		/// Edits all rest nodes of schema.
		/// </summary>
		/// <param name="node">Node.</param>
		private void EditNode(SchemaNodeData node, int index, bool removable, List<int> toAdd, List<int> toRemove)
		{
			EditorGUILayout.BeginHorizontal("Box");

			node.key = (NodeType)EditorGUILayout.EnumPopup(node.key, GUILayout.Width(FIELD_WIDTH));

			node.value = StdTextField(node.value);

			ControlDisplayer defDisplayer = new ControlDisplayer(settingsAsset);
			defDisplayer.DisplayControl(node, FIELD_WIDTH_LARGE);

			SmartTypesPopup popup = new SmartTypesPopup(schemaListAsset, settingsAsset);
			int popupFieldSize = 70;
			if(!popup.SmartChoose(node, popupFieldSize))
			{
				EditorGUILayout.LabelField("", GUILayout.Width(popupFieldSize), GUILayout.ExpandWidth(true));
			}

			if(index >= 0)
			{
				if(removable)
				{
					DeleteButton(index - 1, toRemove);
				}
				ButtonAdd(index, toAdd, "+");
			}

			EditorGUILayout.EndHorizontal();
		}



		/// <summary>
		/// Display Button to add elements to schema.
		/// </summary>
		/// <param name="index">Index.</param>
		/// <param name="dispalyName">Dispaly name.</param>
		/// <param name="size">Size.</param>
		private void ButtonAdd(int index, List<int> toAdd, string dispalyName, int size = 25)
		{
			if(GUILayout.Button(dispalyName, GUILayout.Width(size)))
			{
				toAdd.Add(index);
			}
		}

		/// <summary>
		/// Display button delete with erase schema.
		/// </summary>
		/// <param name="index">Index.</param>
		private void DeleteButton(int index, List<int> toRemove)
		{
			if(GUILayout.Button("-", GUILayout.Width(25)))
			{
				toRemove.Add(index);
			}
		}

		private void DoAdd(List<int> toAdd)
		{
			foreach(int i in toAdd)
			{
				schema.Insert(i, new SchemaNodeData(NodeType.stringType, "name_" + schema.Count));
				SchemaInfoView.SaveStatus = false;
				DatabaseManagerHelper.SetIsGenerated(schemaListAsset, selectedSchemaItem, false);
			}
		}

		private void DoRemove(List<int> toRemove)
		{
			foreach(int i in toRemove)
			{
				schema.Remove(i);
			}
		}

		/// <summary>
		/// Display button with save all new data.
		/// </summary>
		private void SaveButton()
		{
			if(GUILayout.Button("Save", GUILayout.Width(100)))
			{
				schemaListAsset.Save();
				SchemaInfoView.SaveStatus = true;
			}
		}
	}
}
