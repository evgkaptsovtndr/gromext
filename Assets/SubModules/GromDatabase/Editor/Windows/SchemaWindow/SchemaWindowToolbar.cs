using UnityEditor;
using UnityEngine;
using System.Collections;

namespace GromDatabase
{
	public class SchemaWindowToolbar : BasicSchemaWindow
	{

		private CodeValidation validation;
		private int toolbarHeight = 30;

		protected override void OnEnable()
		{
			base.OnEnable();
			validation = new CodeValidation();
		}

		/// <summary>
		/// Displaies the toolbar with options.
		/// </summary>
		public void DisplayToolbar()
		{

			EditorGUILayout.BeginHorizontal("Box", GUILayout.Height(toolbarHeight));

			GUI.SetNextControlName("Create");
			if(GUILayout.Button("Create schema", GUILayout.Height(toolbarHeight)))
			{
				DatabaseManagerHelper.AddNew(schemaListAsset);
				DatabaseManagerHelper.GenerateExterns(schemaListAsset);
				AssetDatabase.Refresh();

				GUI.FocusControl("Create");
			}

			GUI.SetNextControlName("DeleteSchema");
			if(GUILayout.Button("Delete schema", GUILayout.Height(toolbarHeight)) && schemaListAsset.Count > 0)
			{
				SchemaItem toDelete = schemaListAsset.GetElementByIndex<SchemaItem>(selectedSchemaItem);

				if(EditorUtility.DisplayDialog(
					                "Warning!", 
					                System.String.Format("Do you want to delete schema \"{0}\"?", toDelete.GetSchemaName), 
					                "Yes", "No"))
				{
					GUI.FocusControl("DeleteSchema");
					DatabaseManagerHelper.RemoveDatabaseAsset(toDelete);
					DatabaseManagerHelper.Remove(schemaListAsset, selectedSchemaItem);
					DatabaseManagerHelper.GenerateExterns(schemaListAsset);
					AssetDatabase.Refresh();

					selectedSchemaItem--;
					SetSelectedItem();
				}
			}

			GUI.SetNextControlName("DeleteAllSchemas");
			if(GUILayout.Button("Delete all schemas", GUILayout.Height(toolbarHeight)) && schemaListAsset.Count > 0)
			{
				if(EditorUtility.DisplayDialog("Warning!", "Do you want to delete all schemas?", "Yes", "No"))
				{
					GUI.FocusControl("DeleteAllSchemas");
					selectedSchemaItem = 0;
					DatabaseManagerHelper.RemoveAllDatabaseAsset(schemaListAsset);
					DatabaseManagerHelper.RemoveAll(schemaListAsset);
					DatabaseManagerHelper.GenerateExterns(schemaListAsset);
					AssetDatabase.Refresh();
				}
			}

			GUI.SetNextControlName("GenerateSchema");
			if(GUILayout.Button("Generate Schema", GUILayout.Height(toolbarHeight)) && schemaListAsset.Count > 0)
			{
				GUI.FocusControl("GenerateSchema");
				if(validation.Validation(schemaListAsset, selectedSchemaItem))
				{
					DatabaseManagerHelper.GenerateSchema(schemaListAsset, selectedSchemaItem, settingsAsset);
					DatabaseManagerHelper.GenerateExterns(schemaListAsset);

					AssetDatabase.Refresh();
				}

			}

			GUI.SetNextControlName("GenerateAllSchemas");
			if(GUILayout.Button("Generate All Schemas", GUILayout.Height(toolbarHeight)) && schemaListAsset.Count > 0)
			{
				GUI.FocusControl("GenerateAllSchemas");
				if(validation.Validation(schemaListAsset))
				{
					for(int i = 0; i < schemaListAsset.Count; i++)
					{
						DatabaseManagerHelper.GenerateSchema(schemaListAsset, i, settingsAsset);
						DatabaseManagerHelper.GenerateExterns(schemaListAsset);
					}

					AssetDatabase.Refresh();
				}
			}

			EditorGUILayout.EndHorizontal();
		}
	}
}
