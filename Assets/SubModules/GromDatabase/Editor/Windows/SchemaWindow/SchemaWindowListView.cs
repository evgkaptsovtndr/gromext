using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GromDatabase
{
    public class SchemaWindowListView : BasicSchemaWindow
    {

        private int listWidth = 150;


        /// <summary>
        /// Display schemas class as list.
        /// </summary>
        public void DisplayList()
        {

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, "Box", GUILayout.MinWidth(listWidth));

            if (schemaListAsset.Count == 0)
            {

                EditorGUILayout.HelpBox("Schema list is empty", MessageType.Info);
            }
            else
            {
                DisplaySchemaName();
            }

            EditorGUILayout.EndScrollView();
        }

        /// <summary>
        /// Displaies all schemats name.
        /// </summary>
        private void DisplaySchemaName()
        {

            int index = 0;
            foreach (SchemaItem item in schemaListAsset.m_database)
            {
                GUI.SetNextControlName("ListButton " + index);
                if (GUILayout.Button(item.GetSchemaName))
                {
                    selectedSchemaItem = index;
                    GUI.FocusControl("ListButton " + index);
                    GUI.changed = false;
                }
                index++;
            }
        }
    }
}
