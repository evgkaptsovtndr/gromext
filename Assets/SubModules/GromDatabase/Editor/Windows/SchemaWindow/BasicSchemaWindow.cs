using UnityEditor;
using UnityEngine;
using System.Collections;

namespace GromDatabase
{
	public class BasicSchemaWindow : BasicWindow
	{

		protected static int selectedSchemaItem = 0;

		/// <summary>
		/// Sets the selected item index on zero.
		/// </summary>
		protected void SetSelectedItem()
		{
			if(selectedSchemaItem < 0)
			{
				selectedSchemaItem = 0;
			}
		}
	}
}
