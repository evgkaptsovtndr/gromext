﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GromDatabase
{
	public class DatabaseWindowToolbar : BasicDatabaseWindow
	{

		private int toolbarHeight = 30;
		private int selected = 0;

		protected override void OnEnable()
		{
			base.OnEnable();
		}

		/// <summary>
		/// Display part of window where is toolbar
		/// </summary>
		public void DisplayToolbar()
		{
			EditorGUILayout.BeginHorizontal("Box", GUILayout.Height(toolbarHeight));
			DisplayPopupList();
			EditorGUILayout.EndHorizontal();
			if(DatabaseWindowMainView.editMode == false)
			{
				EditorGUILayout.BeginHorizontal("Box", GUILayout.Height(toolbarHeight), GUILayout.ExpandWidth(true));
				DisplayButtons();
				EditorGUILayout.EndHorizontal();
			}
		}

		/// <summary>
		/// Display popup list with generated schemas
		/// </summary>
		private void DisplayPopupList()
		{
			EditorGUI.BeginChangeCheck();
			selected = EditorGUILayout.IntPopup("Databases: ", selected, schemaNames, schemaIndex);
			SetValueFromSelectedIndex();
			if(EditorGUI.EndChangeCheck())
			{
				DatabaseWindowMainView.editMode = false;
				SetDatabase();
			}
		}

		/// <summary>
		/// Get value from index list
		/// </summary>
		private void SetValueFromSelectedIndex()
		{
			if(schemaIndex.Length > 0)
			{
				selectedItem = selected;
			}
			else
			{
				selectedItem = -1;
			}
		}

		/// <summary>
		/// Display buttonts to manage database
		/// </summary>
		private void DisplayButtons()
		{
			if(selectedItem >= 0)
			{
				GUI.SetNextControlName("Add");
				if(GUILayout.Button("Add", GUILayout.Height(toolbarHeight), GUILayout.Width(100)))
				{
					DatabaseContentManager.AddNew(selectedDatabase);
					GUI.FocusControl("Add");
				}
				GUI.SetNextControlName("Delete All");
				if(GUILayout.Button("Delete All", GUILayout.Height(toolbarHeight), GUILayout.Width(100)))
				{
					if(EditorUtility.DisplayDialog("Warning!", "Do you want to delete all database records?", "Yes", "No"))
					{
						DatabaseContentManager.RemoveAll(selectedDatabase);
					}
					GUI.FocusControl("Delete All");
				}
				/*
                GUI.SetNextControlName("Save");
                if (GUILayout.Button("Save", GUILayout.Height(toolbarHeight), GUILayout.Width(100)))
                {
                    DatabaseContentManager.Save(selectedDatabase);
                    GUI.FocusControl("Save");
                }
                 * */
			}
		}
	}
}
