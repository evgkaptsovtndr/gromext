﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;

namespace GromDatabase
{
	public class DatabaseWindowMainView : BasicDatabaseWindow
	{

		public static bool editMode = false;
		private int selectedRecord = -1;
		private int editingDatabase = -1;
		private int byteConvert;
		private GUIStyle centerStyle = new GUIStyle();

		protected override void OnEnable()
		{
			base.OnEnable();
			SetDatabase();
		}

		public void DisplayMainView()
		{
			scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, "Box");
			if(editMode && editingDatabase == selectedItem)
			{
				EditorGUI.BeginChangeCheck();
				DisplayEditor();
				if(EditorGUI.EndChangeCheck())
				{
					DatabaseContentManager.SaveSetDirty(selectedDatabase);
				}
			}
			else
			{
				editingDatabase = -1;
				DisplayTable();
			}
			EditorGUILayout.EndScrollView();
		}

		private void DisplayTable()
		{
			//EditorStyles.helpBox
			EditorGUILayout.BeginHorizontal("Box");
			DisplayColumnName();
			EditorGUILayout.EndHorizontal();

			for(int i = 0; i < DatabaseContentManager.Count(selectedDatabase); ++i)
			{
				EditorGUILayout.BeginHorizontal("Box");
				ButtonEdit(i);
				var isDeleted = ButtonDelete(i);
				if(!isDeleted)
				{
					var item = DatabaseContentManager.GetElement(selectedDatabase, i);

					EditorGUILayout.LabelField(item.id.ToString(), GUILayout.Width(50));
					DisplayContent(i);
					ButtonEdit(i);
					isDeleted = ButtonDelete(i);
				}
				EditorGUILayout.EndHorizontal();

				if(isDeleted)
					--i;
			}
		}

		private void DisplayColumnName()
		{
			EditorGUILayout.LabelField("", GUILayout.Width(40));
			EditorGUILayout.LabelField("ID", GUILayout.Width(50));
			var fields = DatabaseContentManager.GetDatabaseFields(selectedDatabase, schemaListAsset);
			foreach(var field in fields)
			{
				EditorGUILayout.LabelField(field.field.Name, GUILayout.Width(150));
			}
		}

		private void DisplayContent(int index)
		{
			var item = DatabaseContentManager.GetElement(selectedDatabase, index);
			var fields = DatabaseContentManager.GetDatabaseFields(selectedDatabase, schemaListAsset);
			foreach(var field in fields)
			{
				var value = field.field.GetValue(item);
				EditorGUI.BeginDisabledGroup(true);
				DisplayControl(field, ref value, 150);
				EditorGUI.EndDisabledGroup();
				field.field.SetValue(item, value);
			}
		}

		private void DisplayControl(
			DatabaseContentManager.FieldInfoEx field, 
			ref object item, 
			int fieldSize, 
			bool isEditView = false
		)
		{
			ControlDisplayer displayer = new ControlDisplayer(settingsAsset, this);
			displayer.DisplayControl(this, field, ref item, fieldSize, isEditView);
		}

		private void ButtonEdit(int index)
		{
			GUI.SetNextControlName("Ed");
			if(GUILayout.Button("e", GUILayout.Width(20)))
			{
				editingDatabase = selectedItem;
				selectedRecord = index;
				editMode = true;
				GUI.FocusControl("Ed");
			}
		}

		private bool ButtonDelete(int index)
		{
			GUI.SetNextControlName("Del");
			if(GUILayout.Button("-", GUILayout.Width(20)))
			{
				if(EditorUtility.DisplayDialog("Warning!", "Do you want to delete record?", "Yes", "No"))
				{
					DatabaseContentManager.Remove(selectedDatabase, index);
					return true;
				}
			}

			return false;
		}

		private void DisplayEditor()
		{
			int index = 0;
			ButtonBack();

			var fields = DatabaseContentManager.GetDatabaseFields(selectedDatabase, schemaListAsset);
			var item = DatabaseContentManager.GetElement(selectedDatabase, selectedRecord);

			EditorGUILayout.BeginHorizontal("Box");
			EditorGUILayout.LabelField("ID:", GUILayout.Width(100));
			EditorGUILayout.LabelField(item.id.ToString(), GUILayout.Width(280));
			EditorGUILayout.EndHorizontal();

			foreach(var field in fields)
			{
				EditorGUILayout.BeginHorizontal("Box");
				EditorGUILayout.LabelField(field.field.Name + ":", GUILayout.Width(100));
				var value = field.field.GetValue(item);
				DisplayControl(field, ref value, 280, true);
				field.field.SetValue(item, value);
				index++;
				EditorGUILayout.EndHorizontal();
			}
		}

		private void ButtonBack()
		{
			GUI.SetNextControlName("Back");
			if(GUILayout.Button("Back", GUILayout.Width(75)))
			{
				selectedRecord = 0;
				editMode = false;
				GUI.FocusControl("Back");
			}
		}

        

		private void SetGUIStyles()
		{
			centerStyle.alignment = TextAnchor.MiddleCenter;
		}
	}
}
