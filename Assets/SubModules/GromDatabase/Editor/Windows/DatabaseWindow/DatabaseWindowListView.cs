﻿using UnityEditor;
using UnityEngine;
using System.Collections;

namespace GromDatabase
{
	public class DatabaseWindowListView : BasicDatabaseWindow
	{
		private int listWidth = 150;

		public void DisplayListView()
		{
			scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, "Box", GUILayout.MinWidth(listWidth));

			if(schemaListAsset.Count == 0)
			{
				EditorGUILayout.HelpBox("Schema list is empty", MessageType.Info);
			}
			else
			{
				DisplayNames();
			}

			EditorGUILayout.EndScrollView();
		}

		private void DisplayNames()
		{

		}
	}
}
