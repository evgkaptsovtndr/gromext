﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using System.Linq;
using System.Text;

namespace GromDatabase
{
	public class BasicDatabaseWindow : BasicWindow, IDBRefAccessor
	{

		protected static int selectedItem = 0;
		protected static ScriptableObject selectedDatabase;
		protected static string[] schemaNames;
		protected static int[] schemaIndex;

		public class DBInfo
		{
			public ISchema schema = null;
			public string[] allCaptions = null;
			public int[] allValues = null;
		}

		static Dictionary<String, DBInfo> s_referencedDatabases = new Dictionary<String, DBInfo>(8);

		protected virtual void OnGUI()
		{
			GetListOfGeneratedSchemas();
			CreateDatabaseAssets();
		}

		/// <summary>
		/// Sets the selected item index on zero.
		/// </summary>
		protected void SetSelectedItem()
		{
			if(selectedItem < 0)
			{
				selectedItem = 0;
			}
		}

		/// <summary>
		/// Set selected database asset
		/// </summary>
		protected void SetDatabase()
		{
			s_referencedDatabases = new Dictionary<String, DBInfo>(8);

			if(schemaListAsset)
			{
				if(schemaListAsset.Count > 0)
				{
					SchemaItem item = schemaListAsset.GetElementByIndex<SchemaItem>(selectedItem);
					if(item.DatabaseName == "")
					{
						DatabaseManagerHelper.CreateDatabaseAsset(item, schemaListAsset);
					}

					//selectedDatabase = AssetDatabase.LoadAssetAtPath<ScriptableObject>(DataPathHelper.RelativeAssetsDatabasePath + "/" + item.GetSchematName + ".asset");
					selectedDatabase = (ScriptableObject)AssetDatabase.LoadAssetAtPath(DataPathHelper.RelativeAssetsDatabasePath + "/" + item.GetSchemaName + "Database.asset", typeof(ScriptableObject));
				}
			}
		}

		/// <summary>
		/// Get list of generated schemas
		/// </summary>
		protected void GetListOfGeneratedSchemas()
		{
			if(schemaListAsset != null)
			{

				List<string> name = new List<string>();
				List<int> index = new List<int>();

				for(int i = 0; i < schemaListAsset.Count; i++)
				{
					if(schemaListAsset.GetElementByIndex<SchemaItem>(i).isGenerated)
					{
						//name.Add(schemaListAsset.GetElement(i).GetSchematName);
						name.Add(schemaListAsset.GetElementByIndex<SchemaItem>(i).DatabaseName);
						index.Add(i);
					}
				}
				schemaNames = new string[name.Count];
				schemaIndex = new int[index.Count];
				name.CopyTo(schemaNames);
				index.CopyTo(schemaIndex);
			}
		}

		/// <summary>
		/// Create all database with generated schema
		/// </summary>
		protected void CreateDatabaseAssets()
		{
			if(schemaIndex != null)
			{
				foreach(int index in schemaIndex)
				{
					//string name = schemaListAsset.GetElement(index).GetSchematName;
					string name = schemaListAsset.GetElementByIndex<SchemaItem>(index).DatabaseName;
					if(!DatabaseManagerHelper.CheckDatabaseExists(name))
					{
						DatabaseManagerHelper.CreateDatabaseAsset(schemaListAsset.GetElementByIndex<SchemaItem>(index), schemaListAsset);
					}
				}
			}
		}



		/// <summary>
		/// Load all databases
		/// </summary>
		protected DBInfo GetReferencedDB(string dbQualifiedName)
		{
			string dbName = TableRecordRefsUtils.UnqualifiedDBName(dbQualifiedName);

			DBInfo result;

			if(s_referencedDatabases.TryGetValue(dbName, out result))
			{
				return result;
			}

			var schema = AssetDatabase.LoadAssetAtPath(DataPathHelper.RelativeAssetsDatabasePath + "/" + dbName + "Database.asset", typeof(ScriptableObject)) as ISchema;

			if(schema != null)
			{
				result = new DBInfo() {
					schema = schema,
				};

				s_referencedDatabases.Add(dbName, result);
			}

			return result;
		}

#region IDBRefAccessor implementation

		public object GetRecordById(string dbName, int id, Type type)
		{
			var db = GetReferencedDB(dbName);
			if(db == null)
			{
				return null;
			}

			try
			{
				return db.schema.GetElementById(id, type);
			}
			catch(ArgumentException)
			{
				// It's OK - probably, a new element has added to a list
				return null;
			}
		}

		public void GetRecordReferences(string dbName, out string[] captions, out int[] values)
		{
			var db = GetReferencedDB(dbName);
			if(db == null)
			{
				captions = null;
				values = null;
				return;
			}

			if(db.allCaptions == null)
			{
				db.allCaptions = new string[db.schema.Count];
				db.allValues = new int[db.schema.Count];

				for(int i = 0, iMax = db.schema.Count; i < iMax; ++i)
				{
					var item = db.schema.GetElementByIndex<IDbItem>(i);
					db.allCaptions[i] = item.ToString();
					db.allValues[i] = item.id;
				}
			}

			captions = db.allCaptions;
			values = db.allValues;
		}

#endregion
	}
}
