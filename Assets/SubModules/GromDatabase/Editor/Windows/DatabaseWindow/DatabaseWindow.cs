﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System;

namespace GromDatabase
{
	public class DatabaseWindow : BasicDatabaseWindow
	{

		//private DatabaseWindowListView listView;
		private DatabaseWindowMainView mainView;
		private DatabaseWindowToolbar toolbar;

		/// <summary>
		/// Creates and show database window.
		/// </summary>
		[MenuItem("Window/GromDatabase/DatabaseWindow")]
		public static void CreateDatabaseWindow()
		{

			DatabaseWindow window = EditorWindow.GetWindow<DatabaseWindow>("DatabaseWindow");
			window.minSize = new Vector2(600, 400);
		}


		protected override void OnEnable()
		{
			base.OnEnable();
			GetListOfGeneratedSchemas();
			CreateDatabaseAssets();
			CreateInstanceView();
		}

		private void CreateInstanceView()
		{
			mainView = ScriptableObject.CreateInstance<DatabaseWindowMainView>();
			toolbar = ScriptableObject.CreateInstance<DatabaseWindowToolbar>();
		}

		protected override void OnGUI()
		{
			if(EditorApplication.isCompiling == false)
			{
				base.OnGUI();

				if(CheckAssetHaveContentToShow())
				{
					PrintContent();
				}
				else
				{
					PrintInfoMessage();
				}
			}
			else
			{
				PrintInfoCompiling();
			}
		}

		private void PrintInfoCompiling()
		{
			EditorGUILayout.HelpBox("Files are compiling please wait few seconds.", MessageType.Info);
		}

		private void PrintInfoMessage()
		{
			EditorGUILayout.HelpBox("Please create schemas first. To do this open SchemaWindow.", MessageType.Warning);
		}

		private void PrintContent()
		{
			toolbar.DisplayToolbar();
			EditorGUILayout.BeginHorizontal();
			//listView.DisplayListView();
			mainView.DisplayMainView();
			EditorGUILayout.EndHorizontal();
		}

		private bool CheckAssetHaveContentToShow()
		{
			if(schemaListAsset != null)
			{
				if(schemaListAsset.Count > 0 && CountGeneratedSchema() > 0)
				{
					return true;
				}
			}

			return false;
		}

		private int CountGeneratedSchema()
		{
			return schemaListAsset.m_database.FindAll(item => item.isGenerated == true).Count;
		}

	}
}
