﻿using UnityEditor;
using UnityEngine;
using System;
using System.IO;

namespace GromDatabase
{
	public class FilesystemHelper
	{
		public static void CreateSettingsAsset()
		{
			try
			{
				FilesystemHelper.CreateDirectoryIfNotExists(DataPathHelper.AbsoluteAssetsSettingsPath);

				if(!File.Exists(DataPathHelper.RelativeAssetsSettingsPath))
				{

					GromSettings settingsAsset = ScriptableObject.CreateInstance<GromSettings>();
					AssetDatabase.CreateAsset(settingsAsset, DataPathHelper.RelativeAssetsSettingsPath);
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();

				}
			}
			catch(Exception ex)
			{
				Debug.LogException(ex);
			}
		}


		public static void CreateDirectoryIfNotExists(string path)
		{
			if(!Directory.Exists(path))
			{
				try
				{
					Directory.CreateDirectory(path);
					AssetDatabase.Refresh();
				}
				catch(Exception ex)
				{
					Debug.LogException(ex);
				}
			}
		}
			
	}
}

