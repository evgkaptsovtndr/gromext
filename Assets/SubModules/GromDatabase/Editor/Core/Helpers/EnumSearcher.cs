﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System;

/**
 * Класс ищет enum'ы во всех cs-файлах, в путях которых есть DB/Meta или DB/Enums.
 * Каждый enum должен быть определен в пространстве имен DB.Meta.
 * **/

namespace GromDatabase
{

	public class EnumSearcher
	{

		private static List<string> cache = null;

		public static void ClearCache()
		{
			cache = null;
		}

		private static Regex prepareRegex(GromSettings settings)
		{
			string pattern;
			List<string> subpatterns = new List<string>();
			foreach(string ns in settings.namespaces)
			{
				if(String.IsNullOrEmpty(ns))
					continue;
				
				string[] nss = ns.Split('.');

				if(nss.Length == 0)
					continue;

				pattern = @"(?:namespace\s+(?<ns>" + nss[0] + ")";

				for(int i = 1; i < nss.Length; ++i)
				{
					pattern += @"(?:\.|[^}]*{.*?namespace\s+)(?<ns>" + nss[i] + ")";
				}
				pattern += @"[^{]*{.*?enum\s+(?<name>\w+))";
				subpatterns.Add(pattern);
			}

			pattern = String.Join("|", subpatterns.ToArray());

			if(String.IsNullOrEmpty(pattern))
			{
				pattern = @"enum\s+(?<name>\w+)";
			}

			//Debug.Log(String.Format("Enum searching pattern: \"{0}\"", pattern));

			Regex r = new Regex(pattern, RegexOptions.Multiline | RegexOptions.Singleline);
			return r;
		}

		public static List<string> SearchForEnums(GromSettings settings)
		{

			#if UNITY_EDITOR

			if(cache != null)
			{
				return cache;
			}

			List<string> results = new List<string>();

			string[] paths = UnityEditor.AssetDatabase.GetAllAssetPaths();

			Regex r = null;

			foreach(string path in paths)
			{
				if(path.ToLower().EndsWith(".cs") &&
				    (
				        settings.subpaths.Count == 0 ||
				        settings.subpaths.Count(sp => path.IndexOf(sp) >= 0) > 0
				    ))
				{
					using(StreamReader reader = new StreamReader(path))
					{

						string contents = reader.ReadToEnd();
						reader.Close();

						if(contents.IndexOf("enum") < 0)
							continue;

						if(r == null)
						{
							r = prepareRegex(settings);
						}

						Match match = r.Match(contents);
						while(match.Success)
						{

							Group ns = match.Groups["ns"];
							string nsPrefix = "";
							if(ns.Success)
							{
								foreach(Capture c in ns.Captures)
								{
									nsPrefix += c.Value + ".";
								}
							}

							Group g = match.Groups["name"];
							results.Add(nsPrefix + g.Value);
							match = match.NextMatch();
						}		
					}
				}
			}

			cache = new List<string>(results);
			return results;

			#else

				return new List<string>();

			#endif

		}
	}

}