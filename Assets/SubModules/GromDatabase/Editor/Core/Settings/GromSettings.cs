﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GromDatabase
{
	[Serializable]
	public class GromSettings : ScriptableObject
	{
		public List<string> subpaths = new List<string>();
		public List<string> namespaces = new List<string>();

		void OnEnable()
		{
			if(subpaths.Count == 0)
			{
				subpaths.Add("DB/Enums");
				subpaths.Add("DB/Meta");
			}
			if(namespaces.Count == 0)
			{
				namespaces.Add("DB.Meta");
			}
		}
	}
}

