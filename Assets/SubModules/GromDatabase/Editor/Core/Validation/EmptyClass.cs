﻿using System;
using System.Text.RegularExpressions;

namespace GromDatabase
{
	public class NamespasesPathValidator
	{
		private static Regex regex = new Regex(@"^\w+(?:\.\w+)*$", RegexOptions.IgnoreCase);

		public static bool Validate(string path)
		{
			bool ok = regex.IsMatch (path);
			return ok;
		}
	}
}

