﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Linq;

namespace GromDatabase
{
	public class CodeValidation
	{

		private CodeDomProvider provider;
		private List<string> logValidation;

		public CodeValidation()
		{
			provider = new CSharpCodeProvider();
			logValidation = new List<string>();
		}

		/// <summary>
		/// Gets the get log validation.
		/// </summary>
		/// <value>The get log.</value>
		public List<string> GetLog
		{
			get
			{
				return logValidation;
			}
		}

		/// <summary>
		/// Validation schema list. Return true on success, false on errors.
		/// </summary>
		/// <param name="list">List.</param>
		public bool Validation(SchemaList list)
		{
			ClearLog();
			ValidationList(list);
			CheckClassNames(list);

			if(logValidation.Count == 0)
			{
				SuccessValidation();
				return true;
			}
			else
			{
				FailedValidation();
				return false;
			}
		}

		/// <summary>
		/// Validation the specified SchemaItem by index. Return true on success, false on errors.
		/// </summary>
		/// <param name="list">List.</param>
		/// <param name="index">Index.</param>
		public bool Validation(SchemaList list, int index)
		{
			SchemaItem schema = list.GetElementByIndex<SchemaItem>(index);
			ClearLog();
			ValidationSchema(schema);
			CheckClassNames(list);

			if(logValidation.Count == 0)
			{
				SuccessValidation();
				return true;
			}
			else
			{
				FailedValidation();
				return false;
			}
		}

		private void ClearLog()
		{
			if(logValidation.Count > 0)
			{
				logValidation.Clear();
			}
		}

		/// <summary>
		/// Validations the list.
		/// </summary>
		/// <param name="list">List.</param>
		private void ValidationList(SchemaList list)
		{

			foreach(SchemaItem schema in list.m_database)
			{
				ValidationSchema(schema);
			}
		}

		/// <summary>
		/// Validations single schema.
		/// </summary>
		/// <param name="schema">Schema.</param>
		private void ValidationSchema(SchemaItem schema)
		{
			int i = 0;

			if(schema.Count < 2)
			{
				AddLog("Schema '" + schema.GetSchemaName + "' must have at least one variable.");
			}

			foreach(SchemaNodeData node in schema.Get)
			{
				if(!provider.IsValidIdentifier(node.value))
				{
					if(i == 0)
					{
						AddLog("Class name: " + node.value + " is not valid.");
					}
					else
					{
						AddLog("Variable name: " + node.value + "is not valid.");
					}
				}
				i++;
			}

			CheckVariableNames(schema);
		}

		/// <summary>
		/// Adds errors to log.
		/// </summary>
		/// <param name="log">Log.</param>
		private void AddLog(string log)
		{
			logValidation.Add(log);
		}

		/// <summary>
		/// Checks the variable names in schema.
		/// </summary>
		/// <param name="schema">Schema.</param>
		private void CheckVariableNames(SchemaItem schema)
		{
			List<string> result = new List<string>();

			foreach(SchemaNodeData node in schema.Get)
			{
				string name = node.value;

				if((schema.Get.FindAll(nodes => nodes.value == name)).Count > 1 && !result.Contains(name))
				{
					result.Add(name);
				}
			}

			foreach(string s in result)
			{
				AddLog("Name: '" + s + "' occurs multiple times, please fix it before generating code.");
			}
		}

		/// <summary>
		/// Checks the class names in SchemaList.
		/// </summary>
		/// <param name="list">List.</param>
		private void CheckClassNames(SchemaList list)
		{
			List<string> result = new List<string>();
			List<SchemaItem> temp = new List<SchemaItem>();

			foreach(SchemaItem schema in list.m_database)
			{
				string name = schema.GetSchemaName;
				temp = list.m_database.FindAll(item => item.GetSchemaName == schema.GetSchemaName);

				if(temp.Count > 1 && !result.Contains(name))
				{
					result.Add(name);
				}
			}

			foreach(string s in result)
			{
				AddLog("Name schema: '" + s + "' occurs multiple times, please fix it before generating code.");
			}
		}

		/// <summary>
		/// Successes the validation.
		/// </summary>
		private void SuccessValidation()
		{
			Debug.Log("Code generation successfully completed.");
		}

		/// <summary>
		/// Faileds the validation.
		/// </summary>
		private void FailedValidation()
		{
			Debug.Log("Code generation failed completed, please fix errors below.");
			foreach(string log in logValidation)
			{
				Debug.LogWarning(log);
			}
		}
	}
}
