﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;

namespace GromDatabase
{
	public static class DatabaseContentManager
	{
		public class FieldInfoEx
		{
			public SchemaNodeData data = null;
			public FieldInfo field = null;
		}

		private static IDbItem GetGenericItem(ScriptableObject database)
		{
			Type databaseType = database.GetType();
			var getPropertyInfo = databaseType.GetField("m_database");
			object databaseList = getPropertyInfo.GetValue(database);
			Type[] genericTypes = databaseList.GetType().GetGenericArguments();
			Type itemListType = genericTypes[0];

			return Activator.CreateInstance(itemListType) as IDbItem;
		}

		/**
		 * Auxilary method
		 */
		private static FieldInfo[] GetPublicFieldsInfo(ScriptableObject database, out Type itemListType)
		{
			Type databaseType = database.GetType();
			var getPropertyInfo = databaseType.GetField("m_database");
			var databaseList = getPropertyInfo.GetValue(database);

			Type[] genericTypes = databaseList.GetType().GetGenericArguments();
			itemListType = genericTypes[0];
			return genericTypes[0].GetFields(BindingFlags.Public | BindingFlags.Instance);
		}

		public static List<FieldInfoEx> GetDatabaseFields(ScriptableObject database, SchemaList schemaList)
		{
			var result = new List<FieldInfoEx>();

			Type itemListType;
			FieldInfo[] itemFieldsInfo = GetPublicFieldsInfo(database, out itemListType);

			SchemaItem schema = null;

			for(int i = 0; i < schemaList.Count; i++)
			{
				if(schemaList.m_database[i].GetSchemaName == itemListType.Name)
				{
					schema = schemaList.m_database[i];
					break;
				}
			}

			Debug.Assert(schema != null);

			for(int i = 1; i < schema.Count; i++)
			{
				var fieldDesc = schema.Get[i];

				var fieldInfo = itemFieldsInfo.FirstOrDefault(x => x.Name == fieldDesc.value);
				Debug.Assert(fieldInfo != null);
				
				result.Add(new FieldInfoEx() {
					data = fieldDesc,
					field = fieldInfo,
				});
			}

			return result;
		}

		public static void Save(ScriptableObject database)
		{
			Type databaseType = database.GetType();
			databaseType.GetMethod("Save").Invoke(database, null);
		}

		public static void SaveSetDirty(ScriptableObject database)
		{
			Type databaseType = database.GetType();
			databaseType.GetMethod("SaveSetDirty").Invoke(database, null);
		}

		public static void AddNew(ScriptableObject database)
		{
			Type databaseType = database.GetType();
			var item = GetGenericItem(database);

			var prop_nextItemId = databaseType.GetField("nextItemId");

			var nextItemId = (int)prop_nextItemId.GetValue(database);

			item.id = nextItemId;

			prop_nextItemId.SetValue(database, nextItemId + 1);

			var db = database as ISchema;
			db.Add<IDbItem>(item);
		}

		public static void Insert(ScriptableObject database, int index)
		{
			var item = GetGenericItem(database);
			var db = database as ISchema;
			db.Insert<IDbItem>(index, item);
		}

		public static void Insert(ScriptableObject database, int index, IDbItem item)
		{
			var db = database as ISchema;
			db.Insert<IDbItem>(index, item);
		}

		public static void Remove(ScriptableObject database, int index)
		{
			var db = database as ISchema;
			db.Remove<IDbItem>(index);
		}

		public static void Remove(ScriptableObject database, IDbItem item)
		{
			var db = database as ISchema;
			db.Remove<IDbItem>(item);
		}

		public static void RemoveAll(ScriptableObject database)
		{
			var db = database as ISchema;
			db.RemoveAll();
		}

		public static IDbItem GetElement(ScriptableObject database, int index)
		{
			var db = database as ISchema;
			return db.GetElementByIndex<IDbItem>(index);
		}

		public static int Count(ScriptableObject database)
		{
			var db = database as ISchema;
			return db.Count;
		}
	}
}
