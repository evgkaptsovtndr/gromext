﻿using System;
using System.Linq;

namespace GromDatabase
{
	namespace Ext
	{

		public static class GeneratorNamesExt
		{
			public static string Capitalize(this string s)
			{
				if(String.IsNullOrEmpty(s))
					return s;
				return s.First().ToString().ToUpper() + s.Substring(1);
			}


			public static string Add_m_Prefix(this string s)
			{
				if(String.IsNullOrEmpty(s))
					return s;
				if(!s.StartsWith("m_"))
				{
					return "m_" + s;
				}
				return s;
			}

			public static string Remove_m_Prefix(this string s)
			{
				if(String.IsNullOrEmpty(s) || !s.StartsWith("m_"))
					return s;
				return s.Substring(2);
			}

			public static string RemoveId(this string s)
			{
				return RemoveFromEnd(s, "id");
			}

			public static string RemoveIds(this string s)
			{
				return RemoveFromEnd(s, "ids");
			}

			public static string ManyToOne(this string s)
			{
				return RemoveFromEnd(s, "s");
			}


			public static string RemoveFromEnd(this string s, string what)
			{
				if(String.IsNullOrEmpty(s) || s.Length < (what.Length + 1) || what.Length > s.Length)
					return s;

				if(s.ToLower().Substring(s.Length - what.Length).Equals(what))
				{
					return s.Substring(0, s.Length - what.Length);
				}
				return s;
			}


		}

	}

}

