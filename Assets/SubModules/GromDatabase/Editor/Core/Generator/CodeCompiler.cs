﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Reflection;
using Microsoft.CSharp;
using System.IO;

namespace GromDatabase
{
	public class CodeCompiler
	{
		static object locker = new object();

		public void Compile(string savePath, CodeCompileUnit codeCompileUnit)
		{
			CSharpCodeProvider provider = new CSharpCodeProvider();

			try
			{
				lock(locker)
				{
					using(StreamWriter save = new StreamWriter(savePath))
					{
						provider.GenerateCodeFromCompileUnit(codeCompileUnit, save, new CodeGeneratorOptions());
					}
				}
			}
			catch(IOException expection)
			{
				expection.ToString();
			}
			finally
			{
				using(StreamWriter save = new StreamWriter(savePath))
				{
					provider.GenerateCodeFromCompileUnit(codeCompileUnit, save, new CodeGeneratorOptions());
				}
			}
		}
	}
}

