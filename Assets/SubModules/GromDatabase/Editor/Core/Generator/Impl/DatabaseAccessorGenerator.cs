﻿using UnityEngine;
using System.Collections;
using System.CodeDom;


namespace GromDatabase
{
	using GromDatabase.Ext;

	public class DatabaseAccessorGenerator
	{

		public const string CLASS_NAME = "DatabasesAccessor";
		public const string INST_NAME = "Instance";

		private string className;

		public DatabaseAccessorGenerator(string className)
		{
			this.className = className;
		}

		public CodeMemberField MakeMemberField()
		{
			return new CodeMemberField() {
				Attributes = MemberAttributes.Public,
				Name = FieldName(),
				Type = FieldType()
			};
		}

		public static CodeMemberField MakeInstanceMemberField()
		{
			return MakeInstanceMemberField(INST_NAME.Add_m_Prefix(), MemberAttributes.Private | MemberAttributes.Static, false);
		}

		public static CodeMemberProperty MakeInstanceProperty()
		{
			return new CodeMemberProperty() {
				Attributes = MemberAttributes.Public | MemberAttributes.Static,
				Name = INST_NAME,
				Type = new CodeTypeReference(CLASS_NAME),
				HasGet = true,
				HasSet = false,
				GetStatements = { 
					new CodeMethodReturnStatement(new CodeSnippetExpression(INST_NAME.Add_m_Prefix()))
				}
			};
		}

		public static CodeMemberField MakeInstanceMemberField(
			string name, 
			MemberAttributes attrs,
			bool defaultInit
		)
		{
			CodeMemberField field = new CodeMemberField {
				Attributes = attrs,
				Name = name,
				Type = new CodeTypeReference(CLASS_NAME)
			};

			if(defaultInit)
			{
				field.InitExpression = new CodeSnippetExpression(CLASS_NAME + "." + INST_NAME);
			}

			return field;
		}

		public CodeTypeReference FieldType()
		{
			return new CodeTypeReference(className + "Database");
		}

		public string FieldTypeName()
		{
			return className + "Database";
		}

		public string FieldName()
		{
			return "db" + className;
		}
	}
}


