﻿using UnityEngine;
using System.Collections;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Reflection;
using Microsoft.CSharp;
using System.IO;

namespace GromDatabase
{
	public abstract class BaseCodeGenerator
	{
		public const string USING_GROM_DB = "GromDatabase";
		public const string USING_SYSTEM = "System";
		public const string USING_SYSTEM_COLLECTIONS = "System.Collections";
		public const string USING_SYSTEM_COLLECTIONS_GENERIC = "System.Collections.Generic";
		public const string USING_UNITYENGINE = "UnityEngine";

		protected CodeDomProvider provider;
		protected CodeCompileUnit codeCompileUnit;
		protected CodeNamespace codeNamespace;
		protected CodeTypeDeclaration className;
		protected SchemaItem schema;
		protected GromSettings settings;

		private CodeCompiler compiler = new CodeCompiler();

		protected BaseCodeGenerator(SchemaItem schema, GromSettings settings)
		{
			this.schema = schema;
			this.settings = settings;

			codeCompileUnit = new CodeCompileUnit();
			codeNamespace = new CodeNamespace(DataPathHelper.DATABASE_CLASS_NAMESPACE);
			className = new CodeTypeDeclaration();
			ImportNamespace();
			codeNamespace.Types.Add(className);
			codeCompileUnit.Namespaces.Add(codeNamespace);
		}

		public CodeNamespace GetNamespace()
		{
			return codeNamespace;
		}

		private void ImportNamespace()
		{
			codeNamespace.Imports.Add(new CodeNamespaceImport(USING_UNITYENGINE));
			codeNamespace.Imports.Add(new CodeNamespaceImport(USING_SYSTEM_COLLECTIONS));
			codeNamespace.Imports.Add(new CodeNamespaceImport(USING_SYSTEM_COLLECTIONS_GENERIC));
			codeNamespace.Imports.Add(new CodeNamespaceImport(USING_SYSTEM));
			codeNamespace.Imports.Add(new CodeNamespaceImport(USING_GROM_DB));

			foreach(string ns in settings.namespaces)
			{
				codeNamespace.Imports.Add(new CodeNamespaceImport(ns));
			}

		}

		protected virtual void CreateClass()
		{
			//className = new CodeTypeDeclaration();
			CodeAttributeDeclaration serializable = new CodeAttributeDeclaration("Serializable");
			className.CustomAttributes.Add(serializable);
			className.IsClass = true;
			className.IsPartial = true;
			className.TypeAttributes = TypeAttributes.Public;
			AddFields();
		}

		protected abstract void AddFields();

		protected abstract string SavePath();

		public void Compiler()
		{
			compiler.Compile(SavePath(), codeCompileUnit);
		}

	}
}
