﻿
using UnityEngine;
using System;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

namespace GromDatabase
{
	public static class GenericUtils
	{
		public static object GetDefault(Type t) {
			if (t.IsValueType) {
				return Activator.CreateInstance (t);
			}
			return null;
		}

		/**
		 * Try HARD to get a Type by name
		 */
		public static Type GetTypeAtAnyPrice(string qualifiedName)
		{
			string fullType = qualifiedName;

			Type type = Type.GetType (fullType);

			// Unity types:
			if (type == null) {
				fullType = qualifiedName + ", UnityEngine";
				type = Type.GetType (fullType);
			}
			// Enums:
			if (type == null && qualifiedName.IndexOf(".") >= 0) {
				StringBuilder sb = new StringBuilder (qualifiedName);
				sb[qualifiedName.LastIndexOf (".")] = '+';
				fullType = sb.ToString ();
				type = Type.GetType (fullType);
			}

			// Assembly List:
			if (type == null) {
				foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
				{
					foreach (Type t in assembly.GetTypes())
					{
						if (t.FullName == qualifiedName)
							return t;
					}
				}
			}
				
			if (type == null) {
				Debug.LogWarning ("Cannot get type of: " + fullType);
			}

			return type;
		}

		public static Type GetTemplateArg(object aList){
			return aList.GetType ().GetGenericArguments () [0];
		}

		public static Type GetListOfTType(string t)
		{
			Type itemType = GenericUtils.GetTypeAtAnyPrice (t);
			Debug.Assert (itemType != null);

			Debug.Assert (itemType != null);
			Type generic = typeof(List<>);
			return generic.MakeGenericType (new Type[]{ itemType });
		}

		public static void Resize(IList lst, int newSize)
		{
			if(lst.Count == newSize) return;

			while (lst.Count > newSize) {
				lst.RemoveAt (lst.Count - 1);
			}

			if (lst.Count < newSize) {

				Type itemType = GetTemplateArg(lst);

				while (lst.Count < newSize) {
					lst.Add (GenericUtils.GetDefault(itemType));
				}
			}

		}
	}
}

