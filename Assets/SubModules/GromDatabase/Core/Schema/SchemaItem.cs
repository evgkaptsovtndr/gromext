using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace GromDatabase
{
    [Serializable]
	public class SchemaItem : IDbItem
    {

        public List<SchemaNodeData> nodeList = new List<SchemaNodeData>();

		public bool isGenerated = false;
        public string nameToCheck;
        public string databaseName;

        public SchemaItem(int listIndex)
        {
            Add(new SchemaNodeData(NodeType.stringType, "Schema_" + listIndex));
            Add(new SchemaNodeData(NodeType.stringType, "name"));
            isGenerated = false;
            nameToCheck = "Schema_" + listIndex;
        }

        public string DatabaseName
        {
            get
            {
                return databaseName;
            }
            set
            {
                databaseName = value;
            }
        }

        public string NameToCheck
        {
            get
            {
                return nameToCheck;
            }
            set
            {
                nameToCheck = value;
            }
        }

        #region ISchema implementation

        /// <summary>
        /// Add the specified item to list.
        /// </summary>
        /// <param name="item">Item.</param>
        public void Add(SchemaNodeData item)
        {
            nodeList.Add(item);
        }

        /// <summary>
        /// Insert the specified item to list in index position.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <param name="item">Item.</param>
        public void Insert(int index, SchemaNodeData item)
        {
            nodeList.Insert(index, item);
        }

        /// <summary>
        /// Remove the specified item from list.
        /// </summary>
        /// <param name="item">Item.</param>
        public void Remove(SchemaNodeData item)
        {
            nodeList.Remove(item);
        }

        /// <summary>
        /// Remove the specified index from list.
        /// </summary>
        /// <param name="index">Index.</param>
        public void Remove(int index)
        {
            nodeList.RemoveAt(index);
        }

        /// <summary>
        /// Gets the element from list.
        /// </summary>
        /// <returns>The element.</returns>
        /// <param name="index">Index.</param>
        public SchemaNodeData GetElement(int index)
        {
            return nodeList.ElementAt(index);
        }

        /// <summary>
        /// Count item in list.
        /// </summary>
        /// <value>The count.</value>
        public int Count
        {
            get
            {
                return nodeList.Count;
            }
        }

        /// <summary>
        /// Gets nodeList of schemat.
        /// </summary>
        /// <value>The get.</value>
        public List<SchemaNodeData> Get
        {
            get
            {
                return nodeList;
            }
        }

        #endregion

        public string GetSchemaName
        {
            get
            {
                return nodeList.ElementAt(0).value;
            }
        }

		public string GetSchemaQualifiedName
		{
			get
			{
				return String.Format("{0}.{1}", DataPathHelper.DATABASE_CLASS_NAMESPACE, GetSchemaName);
			}
		}

		public int id { get; set; }
	}
}
