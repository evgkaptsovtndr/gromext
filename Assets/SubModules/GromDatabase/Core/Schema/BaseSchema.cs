﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace GromDatabase
{
    [Serializable]
	public abstract class BaseSchema<SchemaType> : ScriptableObject, ISchema where SchemaType : class, IDbItem
    {
		[HideInInspector]
		public int nextItemId = 1;

		public List<SchemaType> m_database = new List<SchemaType>(8);

		// We use relatively slow List<int> istead 
		// of Dictionary for the serialization purposes.
		public List<int> m_idToIndex = new List<int>(8);

#if UNITY_EDITOR
        public void Save()
        {
			m_idToIndex.Clear();
			for(int i = 0; i < m_database.Count; ++i)
			{
				m_idToIndex.Add ( m_database[i].id);
			}

            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
        }

        public void SaveSetDirty()
        {
            EditorUtility.SetDirty(this);
        }

        public void RemoveAll()
        {
			m_database.Clear();
			m_idToIndex.Clear();
            Save();
        }
#endif

#if UNITY_EDITOR
		public void Add<Type>(Type item) where Type : class, IDbItem
        {
			m_database.Add(item as SchemaType);
            Save();
        }

		public void Insert<Type>(int index, Type item) where Type : class, IDbItem
        {
			m_database.Insert(index, item as SchemaType);
            Save();
        }

		public void Remove<Type>(Type item) where Type : class, IDbItem
        {
			m_database.Remove(item as SchemaType);
            Save();
        }

		public void Remove<Type>(int index) where Type : class, IDbItem
        {
			m_database.RemoveAt(index);
            Save();
        }
#endif

		public Type GetElementByIndex<Type>(int index) where Type : class, IDbItem
        {
			return m_database.ElementAt(index) as Type;
        }

		public Type GetElementById<Type>(int id) where Type : class, IDbItem
		{
			return m_database.ElementAt (FindIndexById (id)) as Type;
		}

		public object GetElementById(int id, System.Type type)
		{
			return m_database.ElementAt(FindIndexById(id));
		}

		public bool ContainsElementByIndex<Type>(int index) where Type : class, IDbItem
		{
			return (index >= 0 || index < m_database.Count);
		}

		public bool ContainsElementById<Type>(int id) where Type : class, IDbItem
		{
			try {
				return  ContainsElementByIndex<Type>(FindIndexById (id));
			} catch(ArgumentException){
				return false;
			}
		}

		private int FindIndexById(int id) {
			
			for (int i = 0; i < m_idToIndex.Count; ++i) {
				if (m_idToIndex [i] == id) {
					return i;
				}
			}
			throw new ArgumentException ("Cannot find database record index for id: " + id);
		}


		public int Count
        {
            get
            {
				return m_database.Count;
            }
        }
    }
}
