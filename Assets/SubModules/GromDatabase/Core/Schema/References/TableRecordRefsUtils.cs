﻿using System.Collections;
using System;
using System.Reflection;

namespace GromDatabase
{

	public static class TableRecordRefsUtils {

		public static bool IsTableReferenceType(string type)
		{
			return type.EndsWith (TableRecordRefBase.NAME_POSTFIX);
		}

		public static string TypeToTableReferenceType(string type)
		{
			if (IsTableReferenceType (type))
				return type;
			return type + TableRecordRefBase.NAME_POSTFIX;
		}

		public static string TypeFromTableReferenceType(string type)
		{
			if (!IsTableReferenceType (type) || type.Length <= TableRecordRefBase.NAME_POSTFIX.Length)
				return type;
			
			return type.Substring (0, type.Length - TableRecordRefBase.NAME_POSTFIX.Length);
		}

		public static string UnqualifiedDBName(string qName, bool appendDatabase = false)
		{
			int idx = qName.LastIndexOf (".");
			string dbName = qName;
			if (idx >= 0) {
				dbName = qName.Substring (idx + 1);
			}

			if (appendDatabase) {
				dbName += "Database";
			}

			return dbName;
		}


		public static int GetRecordId(object item) {
			return item != null ? ((TableRecordRefBase) item).recordId : -1;
		}

		public static void SetRecordId(ref object item, IDbItem dbItem, string dbRecordTypeName)
		{
			item = Activator.CreateInstance (GenericUtils.GetTypeAtAnyPrice(dbRecordTypeName));
			((TableRecordRefBase)item).recordId = dbItem.id;
		}

	}
}

