#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace GromDatabase
{
    [Serializable]
    public class SchemaList : BaseSchema<SchemaItem>
    {

        public void SetIsGenerated(int index, bool value)
        {
			m_database.ElementAt(index).isGenerated = value;
#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }
    }
}
