﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

namespace GromDatabase
{
	using GromDatabase.Serialization;

    [Serializable]
	public class SchemaNodeData: IDbItem
    {
		
		public int id { get; set; }

		[SerializeField]
		private NodeType _key;
        
		public string extType;

		public string value;

		public object defValue {
			get {
				return NodeDataSerializer.Deserialize (this, _defValueSerialized);
			}
			set { 
				_defValueSerialized = NodeDataSerializer.Serialize (this, value);
			}
		}

		public bool IsSerializable {
			get { return NodeDataSerializer.isSerializable (key); }
		}

		public NodeType key {
			get {
				return _key;
			}

			set {
				if (_key == value)
					return;
				_key = value;
				defValue = null;
			}
		}

		[SerializeField]
		private string _defValueSerialized;

		public SchemaNodeData(NodeType key, string value, string extType, object defValue)
		{
			this.key = key;
			this.value = value;
			this.extType = extType;
			this.defValue = defValue;
		}

		public SchemaNodeData(NodeType key, string value, string extType)
		{
			this.key = key;
			this.value = value;
			this.extType = extType;
		}

		public SchemaNodeData(NodeType key, string value)
        {
            this.key = key;
            this.value = value;
		}

	}
}
