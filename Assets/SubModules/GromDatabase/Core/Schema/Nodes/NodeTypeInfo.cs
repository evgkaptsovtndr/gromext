﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GromDatabase
{
	public static class NodeTypeInfo
	{
		public static bool IsSystemType(NodeType type){
			return (int) type < 8;
		}
		public static bool IsSystemType(SchemaNodeData node){
			return IsSystemType(node.key);
		}

		/**
		 * returns Ststem.* or UnityEngine.* types
		 */
		public static Type ToStandardType(NodeType type)
		{
			if (IsSystemType (type)) {
				return TranslateToSystemType (type);
			}
			return GenericUtils.GetTypeAtAnyPrice(TranslateToString (type, true));
		}

		public static bool IsTableRectord(SchemaNodeData node) 
		{
			if (IsSystemType (node))
				return false;
			if (String.IsNullOrEmpty (node.extType))
				return false;
			Type type = GenericUtils.GetTypeAtAnyPrice (node.extType);

			if (type != null && type.IsEnum)
				return false;
			// HACK: it's really tricky to detect weather a type is a table reference,
			// so we test its name prefix. 
			// See also: TableRecordRefsUtils::IsTableReferenceType()
			return node.extType.IndexOf(DataPathHelper.DATABASE_CLASS_NAMESPACE) == 0;
		}

		public static List<string> GetTypeNameList()
		{
			return Enum.GetValues (typeof(NodeType))
				.OfType<NodeType> ()
				.ToList ()
				.Select (
				t => IsSystemType (t) 
						? TranslateToSystemType (t).FullName
						: TranslateToString (t, true)).ToList ();
		}

		public static Type TranslateToSystemType(NodeType type)
		{
			Type name = typeof(System.String);

			switch(type)
			{
			case NodeType.charType:
				name = typeof(System.Char);
				break;
			case NodeType.byteType:
				name = typeof(System.Byte);
				break;
			case NodeType.intType:
				name = typeof(System.Int32);
				break;
			case NodeType.floatType:
				name = typeof(System.Single);
				break;
			case NodeType.doubleType:
				name = typeof(System.Double);
				break;
			case NodeType.stringType:
				name = typeof(System.String);
				break;
			case NodeType.boolType:
				name = typeof(System.Boolean);
				break;
				#if UNITY_5_0 || UNITY_5_1
				case NodeType.longType:
				name = typeof(System.Int64);
				break;
				#endif
			}



			return name;
		}

		public static string TranslateToString(NodeType type, bool addNamespace = false)
		{
			string name = "Vector3";

			switch(type)
			{
			case NodeType.vector2Type:
				name = "Vector2";
				break;
			case NodeType.vector3Type:
				name = "Vector3";
				break;
			case NodeType.vector4Type:
				name = "Vector4";
				break;
			case NodeType.colorType:
				name = "Color";
				break;
			case NodeType.gameObjectType:
				name = "GameObject";
				break;
			case NodeType.animationCurveType:
				name = "AnimationCurve";
				break;
			case NodeType.animationClipType:
				name = "AnimationClip";
				break;
			case NodeType.audioClipType:
				name = "AudioClip";
				break;
			case NodeType.boundsType:
				name = "Bounds";
				break;
			case NodeType.layerMaskType:
				name = "LayerMask";
				break;
			case NodeType.materialType:
				name = "Material";
				break;
			case NodeType.rectType:
				name = "Rect";
				break;
			case NodeType.spriteType:
				name = "Sprite";
				break;
			case NodeType.textureType:
				name = "Texture";
				break;
			case NodeType.texture2DType:
				name = "Texture2D";
				break;
			}

			if (addNamespace) {
				return "UnityEngine." + name;
			}

			return name;
		}
	}
}

