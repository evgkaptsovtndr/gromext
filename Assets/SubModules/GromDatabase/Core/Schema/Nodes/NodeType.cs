﻿using UnityEngine;
using System.Collections;

namespace GromDatabase
{
    public enum NodeType
    {
        byteType = 0,
        boolType = 1,
        charType = 2,
        doubleType = 3,
        floatType = 4,
        intType = 5,
#if UNITY_5_0 || UNITY_5_1
        longType = 6,
#endif
        stringType = 7,
        animationCurveType = 8,
        animationClipType = 9,
        audioClipType = 10,
        boundsType = 11,
        colorType = 12,
        gameObjectType = 13,
        layerMaskType = 14,
        materialType = 15,
        rectType = 16,
        spriteType = 17,
        textureType = 18,
        texture2DType = 19,
        vector2Type = 20,
        vector3Type = 21,
        vector4Type = 22,
		
		// specified enum type (dropdown)
		enumType = 23,	

		listType = 24,

		// reference to another database record
		recordReference = 26,
    }
}
