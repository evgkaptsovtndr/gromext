﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace GromDatabase
{
	namespace Serialization {
	
		public class NodeDataSerializer
		{
			private static List<NodeType> serializables = new List<NodeType> {
				NodeType.byteType,
				NodeType.boolType,
				NodeType.charType,
				NodeType.doubleType,
				NodeType.floatType,
				NodeType.intType,
				#if UNITY_5_0 || UNITY_5_1
				NodeType.longType,
				#endif
				NodeType.stringType,
				NodeType.colorType,
				NodeType.layerMaskType,
				NodeType.vector2Type,
				NodeType.vector3Type,
				NodeType.vector4Type,
				NodeType.enumType,
				NodeType.recordReference
			};

			public static bool isSerializable(NodeType type){
				return serializables.Contains (type);			
			}

			public class PrimitiveTypeWrapper {
				public string value;
			}

			public static string Serialize(SchemaNodeData node, object value) {

				if (value == null) {
					return null;
				}

				if (value.GetType ().IsEnum || value.GetType ().IsPrimitive || node.key == NodeType.stringType) {
					return JsonUtility.ToJson (new PrimitiveTypeWrapper{value = value.ToString()});
				} else {
					return JsonUtility.ToJson (value);
				}
			}

			public static object Deserialize(SchemaNodeData node, string value) {

				if (String.IsNullOrEmpty (value))
					return value;

				switch (node.key) {
				case NodeType.byteType:
					return Byte.Parse(JsonUtility.FromJson<PrimitiveTypeWrapper> (value).value);
				case NodeType.boolType:
					return Boolean.Parse(JsonUtility.FromJson<PrimitiveTypeWrapper> (value).value);
				case NodeType.charType:
					string str = JsonUtility.FromJson<PrimitiveTypeWrapper> (value).value;
					return String.IsNullOrEmpty(str) ? default(Char) : str[0];
				case NodeType.doubleType:
					return Double.Parse(JsonUtility.FromJson<PrimitiveTypeWrapper> (value).value);
				case NodeType.floatType:
					return Single.Parse(JsonUtility.FromJson<PrimitiveTypeWrapper> (value).value);
					#if UNITY_5_0 || UNITY_5_1
					case NodeType.longType:
					return Long.Parse(JsonUtility.FromJson<PrimitiveTypeWrapper> (value).value);
					#endif
				case NodeType.stringType:
					return JsonUtility.FromJson<PrimitiveTypeWrapper> (value).value;
				case NodeType.intType:
					return Int32.Parse(JsonUtility.FromJson<PrimitiveTypeWrapper> (value).value);
				case NodeType.colorType:
					return JsonUtility.FromJson<Color> (value);	
				case NodeType.animationCurveType:
					return JsonUtility.FromJson<AnimationCurve> (value);	
				case NodeType.animationClipType:
					return JsonUtility.FromJson<AnimationClip> (value);	
				case NodeType.audioClipType:
					return JsonUtility.FromJson<AudioClip> (value);	
				case NodeType.boundsType:
					return JsonUtility.FromJson<Bounds> (value);	
				case NodeType.gameObjectType:
					return JsonUtility.FromJson<GameObject> (value);	
				case NodeType.layerMaskType:
					return JsonUtility.FromJson<LayerMask> (value);	
				case NodeType.materialType:
					return JsonUtility.FromJson<Material> (value);	
				case NodeType.rectType:
					return JsonUtility.FromJson<Rect> (value);	
				case NodeType.spriteType:
					return JsonUtility.FromJson<Sprite> (value);	
				case NodeType.textureType:
					return JsonUtility.FromJson<Texture> (value);	
				case NodeType.texture2DType:
					return JsonUtility.FromJson<Texture2D> (value);	
				case NodeType.vector2Type:
					return JsonUtility.FromJson<Vector2> (value);	
				case NodeType.vector3Type:
					return JsonUtility.FromJson<Vector3> (value);	
				case NodeType.vector4Type:
					return JsonUtility.FromJson<Vector4> (value);	
				case NodeType.enumType:
					Type enumType = Type.GetType (node.extType);
					return Enum.Parse(enumType, JsonUtility.FromJson<PrimitiveTypeWrapper> (value).value);
				case NodeType.recordReference:
					return Int32.Parse(JsonUtility.FromJson<PrimitiveTypeWrapper> (value).value);
				}

				return value;
			}

		}
	}
}
