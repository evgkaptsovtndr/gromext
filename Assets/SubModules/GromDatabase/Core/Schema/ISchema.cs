﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GromDatabase
{
	public interface ISchema
    {

#if UNITY_EDITOR
		void Add<Type>(Type item) where Type : class, IDbItem;
		void Insert<Type>(int index, Type item) where Type : class, IDbItem;
		void Remove<Type>(Type item) where Type : class, IDbItem;
		void Remove<Type>(int index) where Type : class, IDbItem;

		void RemoveAll();
#endif

        int Count { get; }

		Type GetElementByIndex<Type>(int index) where Type : class, IDbItem;
		Type GetElementById<Type>(int id) where Type : class, IDbItem;

		bool ContainsElementByIndex<Type>(int index) where Type : class, IDbItem;
		bool ContainsElementById<Type>(int id) where Type : class, IDbItem;

		object GetElementById(int id, System.Type type);
	}
}
