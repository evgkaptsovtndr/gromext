﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GromDatabase
{
    public interface IDbItem
    {
		int id { get; set; }
    }
}
