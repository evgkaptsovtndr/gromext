﻿using System;
namespace DB
{
	namespace Meta
	{
		public enum UpgradeClass
		{
			Engine,
			Body,
			Transmission,
			Suspension
		}
	}
}