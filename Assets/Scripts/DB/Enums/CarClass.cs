﻿using System;
namespace DB
{
	namespace Meta
	{
		public enum CarClass
		{
			Touring,
			Muscle,
			Jeeps,
			JDM,
			SportCars,
		}
	}
}