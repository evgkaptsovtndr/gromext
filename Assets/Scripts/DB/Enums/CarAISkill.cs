﻿namespace DB
{
	namespace Meta
	{
		public enum CarAISkill
		{
			NotSet = 0,
			VeryEasy = 1,
			Easy = 2,
			BelowNormal = 3,
			Normal = 4,
			AboveNormal = 5,
			Hard = 6,
			VeryHard = 7
		}
	}
}