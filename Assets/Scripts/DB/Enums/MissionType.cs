﻿using System;
namespace DB
{
	namespace Meta
	{
		public enum MissionType
		{
			Mission_Safety,
			Mission_Drift,
			Mission_Acceleration,
			Mission_Duel,
			Mission_Race,
			Mission_PoliceCar,
		}
	}
}