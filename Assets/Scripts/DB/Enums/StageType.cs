﻿using System;
namespace DB
{
	namespace Meta
	{
		public enum StageType
		{
			Race,			// normal race with AI
			Knockout,		// knockout race
			SoloRace,		// player only
			Duel,			// race with one AI
		}
	}
}