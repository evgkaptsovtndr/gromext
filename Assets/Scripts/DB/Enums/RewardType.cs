﻿using System;
namespace DB
{
	namespace Meta
	{
		public enum RewardType
		{
			None,
			OneStar = 1,
			TwoStars = 2,
			ThreeStars = 3,
		}
	}
}