﻿using System;
namespace DB
{
	namespace Meta
	{
		public enum UpgradeName
		{
			Turbo,
			Ecu,
			PistonBlock,
			Exhaust,
			Crankshaft,

			Clutch,
			Differential,
			Gearbox,

			Dampers,
			AntiRoll,
			Packers,
			Wheels,

			Frame,
			Hood,
			Spoiler,
			Bumper,

			DropGold,
			DropFuel,
			DropCar,
			DropColor,
			DropBooster,

			DropRim,

			None,
		}
	}
}