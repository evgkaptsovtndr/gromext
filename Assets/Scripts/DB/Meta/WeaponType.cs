using UnityEngine;
using System.Collections;

namespace DB
{

    namespace Enums
    {

        public enum WeaponType
        {
            Melee,
            Missile
        }

    }

}
