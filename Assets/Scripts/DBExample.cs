﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

using DB.Meta;

using UnityEngine;

public class DBExample : MonoBehaviour
{

	public HeroDatabase heroes;
	public WeaponDatabase weapons;
	public LocationDatabase locations;


	void Awake()
	{
		DatabasesAccessor.Instance.dbHero = heroes;
		DatabasesAccessor.Instance.dbWeapon = weapons;
		DatabasesAccessor.Instance.dbLocation = locations;
		
		for(int i = 0; i < heroes.Count; ++i)
		{
			Hero hero = heroes.GetElementByIndex<Hero>(i);
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("Hero: " + hero.name);

			if(hero.HasLocation())
			{
				sb.AppendLine("Location: " + hero.GetLocation().name);
			}

			Weapon mainWeapon = hero.GetMainWeapon();
			if(hero.HasMainWeapon())
			{
				sb.AppendLine("Weapon: " + mainWeapon.name);
			}

			foreach(Weapon w in hero.GetWeapons ())
			{
				sb.AppendLine("Additional weapon: " + w.name);	
			}

			if(hero.HasSibling())
			{
				sb.AppendLine("Sibling: " + hero.GetSibling().name);
			}

			sb.AppendLine("------------------------");
			print(sb.ToString());
		}
	}
	
}
