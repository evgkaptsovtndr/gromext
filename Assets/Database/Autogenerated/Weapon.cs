// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 2.0.50727.1433
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace DB.Meta {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using System;
    using GromDatabase;
    using DB.Meta;
    using DB.Enums;
    
    
    [Serializable()]
    public partial class Weapon : IDbItem {
        
        private DatabasesAccessor m_DBAccessorRef = DatabasesAccessor.Instance;
        
        public string name;
        
        [SerializeField()]
        public int location = 0;
        
        public bool HasLocation(DB.Meta.LocationDatabase db = null) {
			if(db == null){ db = m_DBAccessorRef.dbLocation; };
			return db.ContainsElementById<DB.Meta.Location>(location);
        }
        
        public DB.Meta.Location GetLocation(DB.Meta.LocationDatabase db = null) {
			if(db == null){ db = m_DBAccessorRef.dbLocation; };
			return db.GetElementById<DB.Meta.Location>(location);
        }
        
        public override string ToString() {
			return "#" + id + ": " + name;
        }
    }
}
